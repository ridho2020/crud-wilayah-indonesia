<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubDistricts extends Model
{
    use HasFactory;
    protected $table = 'subdistricts';
    protected $fillable = [
        'province_id','district_id','name', 'slug'
    ];

    public function province()
    {
        return $this->belongsTo(Provinces::class);
    }

    public function district()
    {
        return $this->belongsTo(Districts::class);
    }

        public function villages()
    {
        return $this->hasMany(Villages::class);
    }


}
