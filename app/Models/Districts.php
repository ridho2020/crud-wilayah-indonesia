<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    use HasFactory;
    protected $table = 'districts';
    protected $fillable = [
        'province_id','name', 'slug'
    ];

    public function province()
    {
        return $this->belongsTo(Provinces::class);
    }

    public function subdistricts()
    {
        return $this->hasMany(Subdistrict::class);
    }

    public function villages()
    {
        return $this->hasMany(Villages::class);
    }


}

