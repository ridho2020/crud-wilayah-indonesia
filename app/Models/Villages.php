<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Villages extends Model
{
    use HasFactory;
    protected $table = 'villages';
    protected $fillable = ['name', 'slug', 'province_id', 'district_id', 'subdistrict_id'];

    public function province()
    {
        return $this->belongsTo(Provinces::class);
    }

    public function district()
    {
        return $this->belongsTo(Districts::class);
    }

    public function subdistrict()
    {
        return $this->belongsTo(Subdistricts::class);
    }

}
