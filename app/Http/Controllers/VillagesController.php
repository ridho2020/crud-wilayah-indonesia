<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provinces;
use App\Models\Districts;
use App\Models\SubDistricts;
use App\Models\Villages;
use Illuminate\Support\Str;
class VillagesController extends Controller
{
    public function index()
    {
        $villages = Villages::with('province', 'district', 'subdistrict')->latest()->paginate(10);
        // dd($districts);
        return view('village.index', compact('villages'));
    }

   

}
