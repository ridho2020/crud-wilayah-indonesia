<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provinces;
use App\Models\Districts;
use Illuminate\Support\Str;
class DistrictsController extends Controller
{
    public function index()
    {
        $districts = Districts::with('province')->latest()->paginate(10);
        // dd($districts);
        return view('district.index', compact('districts'));
    }

    public function create()
    {
        $provinces = Provinces::get();
        return view('district.create', compact('provinces'));
    }

    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'province_id' => 'required',
            'name'     => 'required'
        ]);

        //slug
        $slug = $request->slug ?? Str::slug($request->name, '-');
        $districts = Districts::create([
            'province_id' => $request->province_id,
            'name'     => $request->name,
            'slug'   => $slug
        ]);

        if($districts){
            //redirect dengan pesan sukses
            return redirect()->route('district.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('district.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

}
