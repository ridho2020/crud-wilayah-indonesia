<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provinces;
use Illuminate\Support\Str;

class ProvincesController extends Controller
{
    public function index()
    {
        $provinces = Provinces::latest()->paginate(10);
        return view('province.index', compact('provinces'));
    }

    public function create()
    {
        return view('province.create');
    }

    public function store(Request $request)
    {

        // dd($request);
        $this->validate($request, [
            'name'     => 'required'
        ]);

        //slug
        $slug = $request->slug ?? Str::slug($request->name, '-');
        $provinces = Provinces::create([
            'name'     => $request->name,
            'slug'   => $slug
        ]);

        if($provinces){
            //redirect dengan pesan sukses
            return redirect()->route('province.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('province.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $provinces = Provinces::find($id);
        return view('province.edit', compact('provinces'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'     => 'required'
        ]);

         $slug = $request->slug ?? Str::slug($request->name, '-');
        //get data Provinces by ID
        $provinces = Provinces::findOrFail($id);

       
            $provinces->update([
                'name'     => $request->name,
                'slug' => $slug
            ]);

        

        if($provinces){
            //redirect dengan pesan sukses
            return redirect()->route('province.index')->with(['success' => 'Data Berhasil Diupdate!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('province.index')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function destroy($id)
    {
    $provinces = Provinces::findOrFail($id);
    $provinces->delete();

    if($provinces){
        //redirect dengan pesan sukses
        return redirect()->route('province.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }else{
        //redirect dengan pesan error
        return redirect()->route('province.index')->with(['error' => 'Data Gagal Dihapus!']);
    }
    }


}
