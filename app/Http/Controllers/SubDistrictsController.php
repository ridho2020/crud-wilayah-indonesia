<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Provinces;
use App\Models\Districts;
use App\Models\SubDistricts;
use Illuminate\Support\Str;
class SubDistrictsController extends Controller
{
    public function index()
    {
        $subdistricts = SubDistricts::with('province', 'district')->latest()->paginate(10);
        // dd($districts);
        return view('subdistrict.index', compact('subdistricts'));
    }

    public function create()
    {
        $provinces = Provinces::get();
        $districts = Districts::get();
        return view('subdistrict.create', compact('provinces', 'districts'));
    }

    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'province_id' => 'required',
            'district_id' => 'required',
            'name'     => 'required'
        ]);

        //slug
        $slug = $request->slug ?? Str::slug($request->name, '-');
        $subdistricts = SubDistricts::create([
            'province_id' => $request->province_id,
            'district_id' => $request->district_id,
            'name'     => $request->name,
            'slug'   => $slug
        ]);

        if($subdistricts){
            //redirect dengan pesan sukses
            return redirect()->route('subdistrict.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('subdistrict.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

}
