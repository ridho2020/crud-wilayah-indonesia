<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProvincesController;
use App\Http\Controllers\DistrictsController;
use App\Http\Controllers\SubDistrictsController;
use App\Http\Controllers\VillagesController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('province', ProvincesController::class);
Route::resource('district', DistrictsController::class);
Route::resource('subdistrict', SubDistrictsController::class);
Route::resource('village', VillagesController::class);